import { NgModule } from '@angular/core';
import { OwmaCoreModule } from './owma.core.module';


@NgModule({
    exports: [
        OwmaCoreModule
    ]
})
export class OwmaModule {

}