import { Entity } from '../../../common/domain-model/entity';


export interface IEntityController<TEntity extends Entity> {
    initializeAsync(entity: TEntity): Promise<void>;
}