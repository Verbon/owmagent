import Dexie from 'dexie';
import { Entity } from '../../domain-model/entity';


export interface IDexieDb {
    getTable<T extends Entity>(type: new() => T): Dexie.Table<T, number>;
}