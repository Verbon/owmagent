import { IDbMigration } from '../../../common/repositories/i-db-migration';
import { City } from '../../domain-model/city';
import { Country } from '../../domain-model/country';
import { WeatherInfo } from '../../domain-model/weather-info';
import { OpenWeatherMapAgentDb } from '../open-weather-map-agent-db';


export class InitialCreate implements IDbMigration {
    private readonly _db: OpenWeatherMapAgentDb;

    private readonly _version: number;


    public readonly name: string;

    public readonly creationDate: Date;


    public constructor(db: OpenWeatherMapAgentDb) {
        this._db = db;
        this._version = 1;

        this.name = 'initial-create';
        this.creationDate = new Date(2017, 8, 3, 16, 25, 0, 0);
    }


    public upAsync(): Promise<void> {
        this._db.version(this._version).stores({
            cities: '++id, &owmId, name, countryId, longitude, latitude',
            countries: '++id, &code',
            weatherHistory: '++id, cityId, temperature, pressure, humidity, windSpeed, clouds, dateTime, sunrise, sunset'
        });

        this._db.cities.mapToClass(City);
        this._db.countries.mapToClass(Country);
        this._db.weatherHistory.mapToClass(WeatherInfo);

        return Promise.resolve();
    }
}