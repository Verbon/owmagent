import { EntityChangeAction } from './entity-change-action';
import { Entity } from '../../../common/domain-model/entity';
import { EventArgs } from '../../../common/events/event-args';


export class EntitiesChangedEventArgs<TEntity extends Entity> extends EventArgs {
    public constructor(
        public readonly entities: TEntity[],
        public readonly entityChangeAction: EntityChangeAction) {
        super();
    }
}