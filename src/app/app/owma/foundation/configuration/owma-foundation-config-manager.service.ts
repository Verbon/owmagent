import { Injectable } from '@angular/core';
import { ApplicationConfigurationService } from '../../../core/configuration/application-configuration.service';


@Injectable()
export class OwmaFoundationConfigManager {
    private static readonly openWeatherMapUrlKey: string = 'openWeatherMapUrl';
    private static readonly openWeatherMapApiKeyKey: string = 'openWeatherMapApiKey';
    private static readonly minWeatherFetchIntervalInSecondsKey: string = 'minWeatherFetchIntervalInSeconds';

    private readonly _applicationConfigurationService: ApplicationConfigurationService;


    public get openWeatherMapUrl(): string { return this.getConfigValue<string>(OwmaFoundationConfigManager.openWeatherMapUrlKey); }

    public get openWeatherMapApiKey(): string { return this.getConfigValue<string>(OwmaFoundationConfigManager.openWeatherMapApiKeyKey); }

    public get minWeatherFetchIntervalInSeconds(): number { return this.getConfigValue<number>(OwmaFoundationConfigManager.minWeatherFetchIntervalInSecondsKey); }


    public constructor(applicationConfigurationService: ApplicationConfigurationService) {
        this._applicationConfigurationService = applicationConfigurationService;
    }


    private getConfigValue<T>(key: string): T {
        return this._applicationConfigurationService.getValue<T>(key);
    }
}