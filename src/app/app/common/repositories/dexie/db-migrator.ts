import { IDbMigration } from '../i-db-migration';
import { IDbMigrator } from '../i-db-migrator';


export class DbMigrator implements IDbMigrator {
    private readonly _migrations: IDbMigration[];

    private _areMigrationsApplied = false;


    public constructor(migrations: IDbMigration[]) {
        this._migrations = migrations;
    }


    public async migrateAsync(): Promise<void> {
        if (this._areMigrationsApplied) {
            return;
        }

        let migrationsCount = this._migrations.length;
        for (let i = 0; i < migrationsCount; i++) {
            let migration = this._migrations[i];
            await migration.upAsync();
        }

        this._areMigrationsApplied = true;
    }
}