import { Injectable } from '@angular/core';
import { City } from '../../domain-model/city';
import { CityControllerFactory } from '../factories/city-controller-factory.service';
import { ICityController } from '../interfaces/i-city-controller';
import { CachingEntityControllerProvider } from './caching-entity-controller-provider';


@Injectable()
export class CityControllerProvider extends CachingEntityControllerProvider<City, ICityController> {
    public constructor(cityControllerFactory: CityControllerFactory) {
        super(cityControllerFactory);
    }
}