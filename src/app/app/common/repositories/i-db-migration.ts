export interface IDbMigration {
    readonly name: string;

    readonly creationDate: Date;


    upAsync(): Promise<void>;
}