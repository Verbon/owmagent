import { Entity } from '../domain-model/entity';
import { IRepository } from './i-repository';


export interface IUnitOfWork {
    getRepository<T extends Entity>(type: new() => T): IRepository<T>;
}