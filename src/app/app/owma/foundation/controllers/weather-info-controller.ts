import * as moment from 'moment';
import { Moment } from '../../../common/moment/moment';
import { WeatherInfo } from '../../domain-model/weather-info';
import { ICityController } from '../interfaces/i-city-controller';
import { IEntityAwareEntityController } from '../interfaces/i-entity-aware-entity-controller';
import { IInternalWeatherInfoService } from '../interfaces/i-internal-weather-info-service';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';


export class WeatherInfoController implements IWeatherInfoController, IEntityAwareEntityController<WeatherInfo> {
    private readonly _internalWeatherInfoService: IInternalWeatherInfoService;

    private _weatherInfo: WeatherInfo;
    private _temperature: number;
    private _pressure: number;
    private _humidityPercentage: number;
    private _windSpeed: number;
    private _cloudsPercentage: number;
    private _dateTime: moment.Moment;
    private _sunrise: moment.Moment;
    private _sunset: moment.Moment;
    private _city: ICityController;


    public get innerEntity(): WeatherInfo { return this._weatherInfo; }

    public get temperature(): number { return this._temperature; }

    public get pressure(): number { return this._pressure; }

    public get humidityPercentage(): number { return this._humidityPercentage; }

    public get windSpeed(): number { return this._windSpeed; }

    public get cloudsPercentage(): number { return this._cloudsPercentage; }

    public get dateTime(): moment.Moment { return this._dateTime; }

    public get sunrise(): moment.Moment { return this._sunrise; }

    public get sunset(): moment.Moment { return this._sunset; }

    public get city(): ICityController { return this._city; }


    public constructor(internalWeatherInfoService: IInternalWeatherInfoService) {
        this._internalWeatherInfoService = internalWeatherInfoService;
    }


    public async initializeAsync(weatherInfo: WeatherInfo): Promise<void> {
        this._weatherInfo = weatherInfo;

        this._temperature = weatherInfo.temperature;
        this._pressure = weatherInfo.pressure;
        this._humidityPercentage = weatherInfo.humidity;
        this._windSpeed = weatherInfo.windSpeed;
        this._cloudsPercentage = weatherInfo.clouds;
        this._dateTime = this.convertToMomentFrom(weatherInfo.dateTime);
        this._sunrise = this.convertToMomentFrom(weatherInfo.sunrise);
        this._sunset = this.convertToMomentFrom(weatherInfo.sunset);

        this._city = await this._internalWeatherInfoService.getCityAsync(weatherInfo);
    }


    private convertToMomentFrom(unixTimestamp: number): moment.Moment {
        return Moment.fromUnix(unixTimestamp);
    }
}