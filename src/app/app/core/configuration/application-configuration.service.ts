import { Injectable } from '@angular/core';
import * as path from 'path';
import ElectronStore = require('electron-store');


@Injectable()
export class ApplicationConfigurationService {
    private _config: ElectronStore;


    public initialize(absoluteConfigPath: string): void {
        if (this._config) {
            throw new Error('Application config has already been initialized.');
        }

        let configFileName = path.basename(absoluteConfigPath, path.extname(absoluteConfigPath));
        let configDirectory = path.dirname(absoluteConfigPath);
        this._config = new ElectronStore({ name: configFileName, cwd: configDirectory });
    }

    public hasValue(key: string): boolean {
        return this._config.has(key);
    }

    public getValue<T>(key: string): T {
        return <T>this._config.get(key);
    }

    public setValue(key: string, value: any): void {
        this._config.set(key, value);
    }

    public removeValue(key: string): void {
        this._config.delete(key);
    }
}