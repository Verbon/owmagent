import * as moment from 'moment';


export class Duration {
    private constructor() {

    }


    public static fromMilliseconds(milliseconds: number): moment.Duration {
        return Duration.createFrom(milliseconds, 'ms');
    }

    public static fromSeconds(seconds: number): moment.Duration {
        return Duration.createFrom(seconds, 's');
    }

    public static fromMinutes(minutes: number): moment.Duration {
        return Duration.createFrom(minutes, 'm');
    }

    public static fromHours(hours: number): moment.Duration {
        return Duration.createFrom(hours, 'h');
    }

    public static fromDays(days: number): moment.Duration {
        return Duration.createFrom(days, 'd');
    }


    private static createFrom(duration: number, unit: 'ms' | 's' | 'm' | 'h' | 'd'): moment.Duration {
        return moment.duration(duration, unit);
    }
}