import { Injectable } from '@angular/core';


@Injectable()
export class JsonSerializer {
    public serialize(object: any): string {
        return JSON.stringify(object);
    }

    public deserialize<T>(serializedObject: string): T {
        return <T> JSON.parse(serializedObject);
    }
}