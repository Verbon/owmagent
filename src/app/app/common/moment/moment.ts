import * as moment from 'moment';
import { Duration } from './duration';


export class Moment {
    public static get now(): moment.Moment {
        return moment();
    }

    public static get utcNow(): moment.Moment {
        return Moment.now.utc();
    }


    private constructor() {

    }


    public static fromUnix(unixTimestamp: number): moment.Moment {
        return moment.unix(unixTimestamp);
    }

    public static fromDate(date: Date): moment.Moment {
        return moment(date);
    }

    public static subtract(first: moment.Moment, second: moment.Moment): moment.Duration {
        let differenceInMilliseconds = first.valueOf() - second.valueOf();
        if(differenceInMilliseconds < 0) {
            throw new Error("When subtracting moments, 1st must be greater than 2nd.");
        }

        return Duration.fromMilliseconds(differenceInMilliseconds);
    }
}