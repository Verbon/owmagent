import { IRepository } from '../../../common/repositories/i-repository';
import { City } from '../../domain-model/city';
import { WeatherInfo } from '../../domain-model/weather-info';


export interface IWeatherInfoRepository extends IRepository<WeatherInfo> {
    getCityLatestWeatherInfoAsync(city: City): Promise<WeatherInfo | null>;
}