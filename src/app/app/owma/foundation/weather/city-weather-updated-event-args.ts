import { EventArgs } from '../../../common/events/event-args';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';


export class CityWeatherUpdatedEventArgs extends EventArgs {
    public constructor(
        public readonly cityWeatherInfoController: IWeatherInfoController) {
        super();
    }
}