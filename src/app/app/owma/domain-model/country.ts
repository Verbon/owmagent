import { Entity } from '../../common/domain-model/entity';


export class Country extends Entity {
    public code: string;
}