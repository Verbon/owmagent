import { Inject, Injectable } from '@angular/core';
import { Lazy } from '../../../common/lazy/lazy';
import { Country } from '../../domain-model/country';
import { LazyInternalCountryServiceInjectionToken } from '../../owma.core.module.configuration';
import { CountryController } from '../controllers/country-controller';
import { ICountryController } from '../interfaces/i-country-controller';
import { IEntityControllerFactory } from '../interfaces/i-entity-controller-factory';
import { IInternalCountryService } from '../interfaces/i-internal-country-service';


@Injectable()
export class CountryControllerFactory implements IEntityControllerFactory<Country, ICountryController> {
    private readonly _lazyInternalCountryService: Lazy<IInternalCountryService>;


    private get internalCountryService(): IInternalCountryService {
        return this._lazyInternalCountryService.value;
    }


    public constructor(
        @Inject(LazyInternalCountryServiceInjectionToken)
        lazyInternalCountryService: Lazy<IInternalCountryService>) {
        this._lazyInternalCountryService = lazyInternalCountryService;
    }


    public async createFromAsync(country: Country): Promise<ICountryController> {
        let countryController = new CountryController(this.internalCountryService);
        await countryController.initializeAsync(country);

        return countryController;
    }
}