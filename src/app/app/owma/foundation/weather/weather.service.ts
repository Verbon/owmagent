import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { AnonymousDisposable } from '../../../common/disposing/anonymous-disposable';
import { IDisposable } from '../../../common/disposing/i-disposable';
import { Entity } from '../../../common/domain-model/entity';
import { Event, IEvent } from '../../../common/events/event';
import { Moment } from '../../../common/moment/moment';
import { IObservable } from '../../../common/observable/i-observable';
import { IObserver } from '../../../common/observable/i-observer';
import { OperationResult } from '../../../common/operation-result';
import { City } from '../../domain-model/city';
import { Country } from '../../domain-model/country';
import { WeatherInfo } from '../../domain-model/weather-info';
import { OpenWeatherMapAgentUnitOfWork } from '../../repositories/open-weather-map-agent-unit-of-work';
import { OwmaWebService } from '../../service-clients/owma-web.service';
import { GetCityLatestWeatherServerResponse } from '../../service-clients/server-responses/get-city-latest-weather-server-response';
import { ServerCity } from '../../service-clients/server-responses/server-city';
import { ServerCountry } from '../../service-clients/server-responses/server-country';
import { OwmaFoundationConfigManager } from '../configuration/owma-foundation-config-manager.service';
import { EntitiesChangedEventArgs } from '../entity-controller-service/entities-changed-event-args';
import { EntityChangeAction } from '../entity-controller-service/entity-change-action';
import { ICityController } from '../interfaces/i-city-controller';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';
import { IWeatherService } from '../interfaces/i-weather-service';
import { CityControllerProvider } from '../providers/city-controller-provider.service';
import { WeatherInfoControllerProvider } from '../providers/weather-info-controller-provider.service';
import { CityWeatherUpdatedEventArgs } from './city-weather-updated-event-args';


@Injectable()
export class WeatherService implements IWeatherService,
                                       IObservable<EntitiesChangedEventArgs<City>>,
                                       IObservable<EntitiesChangedEventArgs<Country>>,
                                       IObservable<EntitiesChangedEventArgs<WeatherInfo>> {
    private readonly _owmaWebService: OwmaWebService;
    private readonly _owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork;
    private readonly _weatherInfoControllerProvider: WeatherInfoControllerProvider;
    private readonly _cityControllerProvider: CityControllerProvider;
    private readonly _owmaFoundationConfigManager: OwmaFoundationConfigManager;

    private readonly _citiesObservers: IObserver<EntitiesChangedEventArgs<City>>[];
    private readonly _countriesObservers: IObserver<EntitiesChangedEventArgs<Country>>[];
    private readonly _weatherInfosObservers: IObserver<EntitiesChangedEventArgs<WeatherInfo>>[];
    private readonly _cityWeatherUpdatedEvent = new Event<CityWeatherUpdatedEventArgs>();


    public get cityWeatherUpdated(): IEvent<CityWeatherUpdatedEventArgs> { return this._cityWeatherUpdatedEvent; }


    public constructor(
        owmaWebService: OwmaWebService,
        owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork,
        weatherInfoControllerProvider: WeatherInfoControllerProvider,
        cityControllerProvider: CityControllerProvider,
        owmaFoundationConfigManager: OwmaFoundationConfigManager) {
        this._owmaWebService = owmaWebService;
        this._owmaUnitOfWork = owmaUnitOfWork;
        this._weatherInfoControllerProvider = weatherInfoControllerProvider;
        this._cityControllerProvider = cityControllerProvider;
        this._owmaFoundationConfigManager = owmaFoundationConfigManager;

        this._citiesObservers = [];
        this._countriesObservers = [];
        this._weatherInfosObservers = [];
    }


    public subscribe(observer: IObserver<EntitiesChangedEventArgs<City>>, parameter?: object | undefined): IDisposable;
    public subscribe(observer: IObserver<EntitiesChangedEventArgs<Country>>, parameter?: object | undefined): IDisposable;
    public subscribe(observer: IObserver<EntitiesChangedEventArgs<WeatherInfo>>, parameter?: object | undefined): IDisposable;
    public subscribe(observer: any, parameter?: object | undefined): IDisposable {
        let entityType = <new () => Entity>parameter;
        switch (entityType.name) {
            case City.name:
                return this.subscribeToCitiesUpdates(<IObserver<EntitiesChangedEventArgs<City>>>observer);
            case Country.name:
                return this.subscribeToCountriesUpdates(<IObserver<EntitiesChangedEventArgs<Country>>>observer);
            case WeatherInfo.name:
                return this.subscribeToWeatherInfosUpdates(<IObserver<EntitiesChangedEventArgs<WeatherInfo>>>observer);
            default:
                throw new Error(`Argument out of range ${entityType.name}`);
        }
    }

    public async getCityLatestWeatherAsync(cityName: string): Promise<OperationResult<IWeatherInfoController>>;
    public async getCityLatestWeatherAsync(cityController: ICityController): Promise<OperationResult<IWeatherInfoController>>;
    public async getCityLatestWeatherAsync(cityNameOrController: any): Promise<OperationResult<IWeatherInfoController>> {
        if (typeof cityNameOrController === 'string') {
            let cityName = <string>cityNameOrController;

            return await this.getCityLatestWeatherByNameAsync(cityName);
        }
        else {
            let cityController = <ICityController>cityNameOrController;

            return await this.getCityLatestWeatherByControllerAsync(cityController);
        }
    }


    private async getCityLatestWeatherByNameAsync(cityName: string): Promise<OperationResult<IWeatherInfoController>> {
        let getCityLatestWeatherResult = await this._owmaWebService.getCityLatestWeatherAsync(cityName);
        if (!getCityLatestWeatherResult.isSuccessful) {
            return OperationResult.createUnsuccessful<IWeatherInfoController>();
        }
        let getCityLatestWeatherServerResponse = getCityLatestWeatherResult.result;
        let weatherInfoController = await this.syncCityLatestWeatherAsync(getCityLatestWeatherServerResponse);

        return OperationResult.createSuccessful(weatherInfoController);
    }

    private async getCityLatestWeatherByControllerAsync(cityController: ICityController): Promise<OperationResult<IWeatherInfoController>> {
        let weatherInfoRepository = this._owmaUnitOfWork.weatherInfoRepository;
        let cityControllerEntity = this._cityControllerProvider.getEntityOf(cityController);
        let dbCityLatestWeatherInfo = await weatherInfoRepository.getCityLatestWeatherInfoAsync(cityControllerEntity);

        let getCityLatestWeatherResult: OperationResult<GetCityLatestWeatherServerResponse>;
        if (dbCityLatestWeatherInfo !== null) {
            let dbWeatherInfoController = await this._weatherInfoControllerProvider.getControllerForAsync(dbCityLatestWeatherInfo);
            let calculationMoment = Moment.fromUnix(dbCityLatestWeatherInfo.dateTime)
            let timeSpentSinceLastFetch = Moment.subtract(Moment.utcNow, calculationMoment);
            if (timeSpentSinceLastFetch.asSeconds() < this._owmaFoundationConfigManager.minWeatherFetchIntervalInSeconds) {
                return OperationResult.createSuccessful<IWeatherInfoController>(dbWeatherInfoController);
            }
            getCityLatestWeatherResult = await this._owmaWebService.getCityLatestWeatherAsync(cityControllerEntity.owmId);
            if (!getCityLatestWeatherResult.isSuccessful) {
                return OperationResult.createSuccessful<IWeatherInfoController>(dbWeatherInfoController);
            }
        }
        else {
            getCityLatestWeatherResult = await this._owmaWebService.getCityLatestWeatherAsync(cityControllerEntity.owmId);
            if (!getCityLatestWeatherResult.isSuccessful) {
                return OperationResult.createUnsuccessful<IWeatherInfoController>();
            }
        }
        let getCityLatestWeatherServerResponse = getCityLatestWeatherResult.result;
        let weatherInfoController = await this.syncCityLatestWeatherAsync(getCityLatestWeatherServerResponse);

        return OperationResult.createSuccessful(weatherInfoController);
    }

    private async syncCityLatestWeatherAsync(serverResponse: GetCityLatestWeatherServerResponse): Promise<IWeatherInfoController> {
        let country = await this.syncCountryAsync(serverResponse);
        let city = await this.syncCityAsync(serverResponse, country);
        let weatherInfo = await this.syncWeatherInfoAsync(serverResponse, city);
        let weatherInfoController = await this._weatherInfoControllerProvider.getControllerForAsync(weatherInfo);

        this.onCityWeatherUpdated(weatherInfoController);

        return weatherInfoController;
    }

    private async syncCountryAsync(serverResponse: GetCityLatestWeatherServerResponse): Promise<Country> {
        let serverCountry = serverResponse.country;
        let countryRepository = this._owmaUnitOfWork.getRepository(Country);
        let dbCountry = await countryRepository.singleOrDefaultAsync(c => c.code === serverCountry.code);
        if (dbCountry !== null) {
            this.updateCountryFrom(serverCountry, dbCountry);
            await countryRepository.updateAsync(dbCountry);
            this.notifyCountriesUpdated([dbCountry]);
        }
        else {
            dbCountry = this.createCountryFrom(serverCountry);
            await countryRepository.addAsync(dbCountry);
            this.notifyCountriesAdded([dbCountry]);
        }

        return dbCountry;
    }

    private async syncCityAsync(serverResponse: GetCityLatestWeatherServerResponse, cityCountry: Country): Promise<City> {
        let serverCity = serverResponse.city;
        let cityRepository = this._owmaUnitOfWork.getRepository(City);
        let dbCity = await cityRepository.singleOrDefaultAsync(c => c.owmId === serverCity.id);
        if (dbCity !== null) {
            this.updateCityFrom(serverCity, dbCity, cityCountry);
            await cityRepository.updateAsync(dbCity);
            this.notifyCitiesUpdated([dbCity]);
        }
        else {
            dbCity = this.createCityFrom(serverCity, cityCountry);
            await cityRepository.addAsync(dbCity);
            this.notifyCitiesAdded([dbCity]);
        }

        return dbCity;
    }

    private async syncWeatherInfoAsync(serverResponse: GetCityLatestWeatherServerResponse, weatherInfoCity: City): Promise<WeatherInfo> {
        let weatherInfoRepository = this._owmaUnitOfWork.getRepository(WeatherInfo);
        let dbWeatherInfo = await weatherInfoRepository.singleOrDefaultAsync(
            wi => wi.cityId === weatherInfoCity.id && wi.dateTime === serverResponse.calculationUnixTimeUTC);
        if (dbWeatherInfo !== null) {
            this.updateWeatherInfoFrom(serverResponse, dbWeatherInfo, weatherInfoCity);
            await weatherInfoRepository.updateAsync(dbWeatherInfo);
            this.notifyWeatherInfosUpdated([dbWeatherInfo]);
        }
        else {
            dbWeatherInfo = this.createWeatherInfoFrom(serverResponse, weatherInfoCity);
            await weatherInfoRepository.addAsync(dbWeatherInfo);
            this.notifyWeatherInfosAdded([dbWeatherInfo]);
        }

        return dbWeatherInfo;
    }

    private createCountryFrom(serverCountry: ServerCountry): Country {
        let country = new Country();
        this.updateCountryFrom(serverCountry, country);

        return country;
    }

    private updateCountryFrom(serverCountry: ServerCountry, country: Country): void {
        country.code = serverCountry.code;
    }

    private createCityFrom(serverCity: ServerCity, cityCountry: Country): City {
        let city = new City();
        city.owmId = serverCity.id;
        this.updateCityFrom(serverCity, city, cityCountry);

        return city;
    }

    private updateCityFrom(serverCity: ServerCity, city: City, cityCountry: Country): void {
        city.countryId = cityCountry.id;
        city.latitude = serverCity.latitude;
        city.longitude = serverCity.longitude;
        city.name = serverCity.name;
    }

    private createWeatherInfoFrom(serverResponse: GetCityLatestWeatherServerResponse, weatherInfoCity: City): WeatherInfo {
        let weatherInfo = new WeatherInfo();
        this.updateWeatherInfoFrom(serverResponse, weatherInfo, weatherInfoCity);

        return weatherInfo;
    }

    private updateWeatherInfoFrom(serverResponse: GetCityLatestWeatherServerResponse, weatherInfo: WeatherInfo, weatherInfoCity: City): void {
        weatherInfo.cityId = weatherInfoCity.id;
        weatherInfo.clouds = serverResponse.cloudsPercentage;
        weatherInfo.dateTime = serverResponse.calculationUnixTimeUTC;
        weatherInfo.humidity = serverResponse.humidityPercentage;
        weatherInfo.pressure = serverResponse.pressure;
        weatherInfo.sunrise = serverResponse.sunriseUnixTimeUTC;
        weatherInfo.sunset = serverResponse.sunsetUnixTimeUTC;
        weatherInfo.temperature = serverResponse.temperature;
        weatherInfo.windSpeed = serverResponse.windSpeed;
    }

    private onCityWeatherUpdated(weatherInfoController: IWeatherInfoController): void {
        this._cityWeatherUpdatedEvent.raise(this, new CityWeatherUpdatedEventArgs(weatherInfoController));
    }

    private notifyCitiesAdded(cities: City[]): void {
        this.notifyCitiesChanged(cities, EntityChangeAction.Add);
    }

    private notifyCitiesUpdated(cities: City[]): void {
        this.notifyCitiesChanged(cities, EntityChangeAction.Update);
    }

    private notifyCountriesAdded(countries: Country[]): void {
        this.notifyCountriesChanged(countries, EntityChangeAction.Add);
    }

    private notifyCountriesUpdated(countries: Country[]): void {
        this.notifyCountriesChanged(countries, EntityChangeAction.Update);
    }

    private notifyWeatherInfosAdded(weatherInfos: WeatherInfo[]): void {
        this.notifyWeatherInfosChanged(weatherInfos, EntityChangeAction.Add);
    }

    private notifyWeatherInfosUpdated(weatherInfos: WeatherInfo[]): void {
        this.notifyWeatherInfosChanged(weatherInfos, EntityChangeAction.Update);
    }

    private notifyCitiesChanged(cities: City[], changeAction: EntityChangeAction): void {
        let citiesChangedEventArgs = new EntitiesChangedEventArgs<City>(cities, changeAction);

        this._citiesObservers.forEach(co => co.onNext(citiesChangedEventArgs));
    }

    private notifyCountriesChanged(countries: Country[], changeAction: EntityChangeAction): void {
        let countriesChangedEventArgs = new EntitiesChangedEventArgs<Country>(countries, changeAction);

        this._countriesObservers.forEach(co => co.onNext(countriesChangedEventArgs));
    }

    private notifyWeatherInfosChanged(weatherInfos: WeatherInfo[], changeAction: EntityChangeAction): void {
        let weatherInfosChangedEventArgs = new EntitiesChangedEventArgs<WeatherInfo>(weatherInfos, changeAction);

        this._weatherInfosObservers.forEach(wio => wio.onNext(weatherInfosChangedEventArgs));
    }

    private subscribeToCitiesUpdates(citiesObserver: IObserver<EntitiesChangedEventArgs<City>>): IDisposable {
        this._citiesObservers.push(citiesObserver);

        return new AnonymousDisposable(() => _.pull(this._citiesObservers, citiesObserver));
    }

    private subscribeToCountriesUpdates(countriesObserver: IObserver<EntitiesChangedEventArgs<Country>>): IDisposable {
        this._countriesObservers.push(countriesObserver);

        return new AnonymousDisposable(() => _.pull(this._countriesObservers, countriesObserver));
    }

    private subscribeToWeatherInfosUpdates(weatherInfosObserver: IObserver<EntitiesChangedEventArgs<WeatherInfo>>): IDisposable {
        this._weatherInfosObservers.push(weatherInfosObserver);

        return new AnonymousDisposable(() => _.pull(this._weatherInfosObservers, weatherInfosObserver));
    }
}