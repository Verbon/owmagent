import { Entity } from '../../../common/domain-model/entity';
import { IEvent } from '../../../common/events/event';
import { EntitiesAddedEventArgs } from '../entity-controller-service/entities-added-event-args';
import { EntitiesDeletedEventArgs } from '../entity-controller-service/entities-deleted-event-args';
import { EntitiesUpdatedEventArgs } from '../entity-controller-service/entities-updated-event-args';
import { IEntityController } from './i-entity-controller';


export interface IEntityControllerService<TEntity extends Entity, TEntityController extends IEntityController<TEntity>> {
    entitiesAdded: IEvent<EntitiesAddedEventArgs<TEntityController>>;

    entitiesUpdated: IEvent<EntitiesUpdatedEventArgs<TEntityController>>;

    entitiesDeleted: IEvent<EntitiesDeletedEventArgs<TEntityController>>;
}