import { Entity } from '../../../common/domain-model/entity';
import { IEntityController } from './i-entity-controller';


export interface IEntityControllerFactory<TEntity extends Entity, TEntityController extends IEntityController<TEntity>> {
    createFromAsync(entity: TEntity): Promise<TEntityController>;
}