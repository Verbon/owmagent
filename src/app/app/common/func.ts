export interface Func<TResult> {
    (): TResult;
}

export interface ParameterizedFunc<T, TResult> {
    (arg: T): TResult;
}