import { Injectable } from '@angular/core';
import { WeatherInfo } from '../../domain-model/weather-info';
import { WeatherInfoControllerFactory } from '../factories/weather-info-controller-factory.service';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';
import { CachingEntityControllerProvider } from './caching-entity-controller-provider';


@Injectable()
export class WeatherInfoControllerProvider extends CachingEntityControllerProvider<WeatherInfo, IWeatherInfoController> {
    public constructor(weatherInfoControllerFactory: WeatherInfoControllerFactory) {
        super(weatherInfoControllerFactory);
    }
}