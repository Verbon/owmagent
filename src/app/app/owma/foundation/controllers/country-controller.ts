import { Country } from '../../domain-model/country';
import { ICityController } from '../interfaces/i-city-controller';
import { ICountryController } from '../interfaces/i-country-controller';
import { IEntityAwareEntityController } from '../interfaces/i-entity-aware-entity-controller';
import { IInternalCountryService } from '../interfaces/i-internal-country-service';


export class CountryController implements ICountryController, IEntityAwareEntityController<Country> {
    private readonly _internalCountryService: IInternalCountryService;

    private _country: Country;
    private _code: string;


    public get innerEntity(): Country { return this._country; }

    public get code(): string { return this._code; }


    public constructor(internalCountryService: IInternalCountryService) {
        this._internalCountryService = internalCountryService;
    }


    public initializeAsync(country: Country): Promise<void> {
        this._country = country;

        this._code = country.code;

        return Promise.resolve();
    }

    public async getCitiesAsync(): Promise<ICityController[]> {
        return await this._internalCountryService.getCitiesAsync(this._country);
    }
}