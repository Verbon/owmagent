import { City } from '../../domain-model/city';
import { ICityController } from '../interfaces/i-city-controller';
import { ICountryController } from '../interfaces/i-country-controller';
import { IEntityAwareEntityController } from '../interfaces/i-entity-aware-entity-controller';
import { IInternalCityService } from '../interfaces/i-internal-city-service';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';


export class CityController implements ICityController, IEntityAwareEntityController<City> {
    private readonly _internalCityService: IInternalCityService;

    private _city: City;
    private _name: string;
    private _latitude: number;
    private _longitude: number;
    private _country: ICountryController;


    public get innerEntity(): City { return this._city; }

    public get name(): string { return this._name; }

    public get latitude(): number { return this._latitude; }

    public get longitude(): number { return this._longitude; }

    public get country(): ICountryController { return this._country; }


    public constructor(internalCityService: IInternalCityService) {
        this._internalCityService = internalCityService;
    }


    public async initializeAsync(city: City): Promise<void> {
        this._city = city;

        this._name = city.name;
        this._latitude = city.latitude;
        this._longitude = city.longitude;
        this._country = await this._internalCityService.getCountryAsync(city);
    }

    public async getWeatherHistoryAsync(): Promise<IWeatherInfoController[]> {
        return await this._internalCityService.getWeatherHistoryAsync(this._city);
    }
}