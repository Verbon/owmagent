import { IDbMigrator } from './i-db-migrator';


export class DatabaseInitializer {
    private static _dbMigrator: IDbMigrator;


    private constructor() {

    }


    public static setMigrator(dbMigrator: IDbMigrator): void {
        if(DatabaseInitializer._dbMigrator) {
            throw new Error("Database migrator is already set.");
        }

        DatabaseInitializer._dbMigrator = dbMigrator;
    }

    public static async initializeAsync(): Promise<void> {
        if(!DatabaseInitializer._dbMigrator) {
            throw new Error("Set database migrator.");
        }

        await DatabaseInitializer._dbMigrator.migrateAsync();
    }
}