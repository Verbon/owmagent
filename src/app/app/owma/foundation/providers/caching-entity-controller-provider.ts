import { Entity } from '../../../common/domain-model/entity';
import { IEntityAwareEntityController } from '../interfaces/i-entity-aware-entity-controller';
import { IEntityController } from '../interfaces/i-entity-controller';
import { IEntityControllerFactory } from '../interfaces/i-entity-controller-factory';
import { IEntityControllerProvider } from '../interfaces/i-entity-controller-provider';


export abstract class CachingEntityControllerProvider<TEntity extends Entity, TEntityController extends IEntityController<TEntity>> implements IEntityControllerProvider<TEntity, TEntityController> {
    private readonly _entityControllerFactory: IEntityControllerFactory<TEntity, TEntityController>;

    private readonly _entityControllersCache: Map<number, TEntityController>;


    protected constructor(entityControllerFactory: IEntityControllerFactory<TEntity, TEntityController>) {
        this._entityControllerFactory = entityControllerFactory;

        this._entityControllersCache = new Map<number, TEntityController>();
    }


    public async getControllerForAsync(entity: TEntity): Promise<TEntityController> {
        let entityKey = entity.id;
        if (!this._entityControllersCache.has(entityKey)) {
            let entityController = await this._entityControllerFactory.createFromAsync(entity);
            this._entityControllersCache.set(entityKey, entityController);
        }
        let entityController = <TEntityController>this._entityControllersCache.get(entityKey);

        return entityController;
    }

    public getEntityOf(entityController: TEntityController): TEntity {
        let entityAwareEntityController = <IEntityAwareEntityController<TEntity>><object>entityController;

        return entityAwareEntityController.innerEntity;
    }
}