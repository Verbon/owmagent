import { WeatherInfo } from '../../domain-model/weather-info';
import { ICityController } from './i-city-controller';
import { IWeatherInfoService } from './i-weather-info-service';


export interface IInternalWeatherInfoService extends IWeatherInfoService {
    getCityAsync(weatherInfo: WeatherInfo): Promise<ICityController>;
}