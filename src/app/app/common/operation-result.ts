export class OperationResult<T> {
    private readonly _result: T;


    public readonly isSuccessful: boolean;

    public get result(): T {
        if(!this.isSuccessful) {
            throw new Error('Operation result is not successful.');
        }

        return this._result;
    }


    private constructor(isSuccessful: boolean, result?: T) {
        this.isSuccessful = isSuccessful;
        if(result) {
            this._result = result;
        }
    }


    public static createUnsuccessful<T>(): OperationResult<T> {
        return new OperationResult<T>(false);
    }

    public static createSuccessful<T>(result: T): OperationResult<T> {
        return new OperationResult<T>(true, result);
    }
}