import { EventArgs } from './event-args';


export interface EventHandler<T extends EventArgs> {
    (sender: object, e: T): void;
}