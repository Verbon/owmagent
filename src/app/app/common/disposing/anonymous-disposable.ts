import { Action } from '../action';
import { IDisposable } from './i-disposable';


export class AnonymousDisposable implements IDisposable {
    private readonly _dispose: Action;


    public constructor(dispose: Action) {
        this._dispose = dispose;
    }


    public dispose(): void {
        this._dispose();
    }
}