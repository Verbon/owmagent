import { Entity } from '../../domain-model/entity';
import { DatabaseInitializer } from '../database-initializer';
import { IDbMigration } from '../i-db-migration';
import { IRepository } from '../i-repository';
import { IUnitOfWork } from '../i-unit-of-work';
import { DbMigrator } from './db-migrator';
import { IDexieDb } from './i-dexie-db';
import { Repository } from './repository';


export class UnitOfWork implements IUnitOfWork {
    protected readonly db: IDexieDb;

    private readonly _repositories: Map<string, object>;


    public constructor(db: IDexieDb, migrations: IDbMigration[]) {
        this.db = db;

        this._repositories = new Map<string, object>();

        let dbMigrator = new DbMigrator(migrations);
        DatabaseInitializer.setMigrator(dbMigrator);
    }


    public getRepository<T extends Entity>(type: new () => T): IRepository<T> {
        let typeName = type.name;
        if (this._repositories.has(typeName)) {
            return <IRepository<T>>this._repositories.get(typeName);
        }

        let repository = this.createRepository(type);
        this._repositories.set(typeName, repository);

        return repository;
    }


    protected createRepository<T extends Entity>(entityType: new () => T): IRepository<T> {
        return new Repository<T>(entityType, this.db);
    }
}