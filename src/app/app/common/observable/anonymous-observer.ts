import { Action, ParameterizedAction } from '../action';
import { IObserver } from './i-observer';


export class AnonymousObserver<T> implements IObserver<T> {
    private readonly _onNext: ParameterizedAction<T>;
    private readonly _onError: ParameterizedAction<Error>;
    private readonly _onCompleted: Action;


    public constructor(onNext: ParameterizedAction<T>, onError?: ParameterizedAction<Error>, onCompleted?: Action) {
        this._onNext = onNext;
        this._onError = onError || (error => { throw error; });
        this._onCompleted = onCompleted || (() => { });
    }


    public onNext(value: T): void {
        this._onNext(value);
    }

    public onError(error: Error): void {
        this._onError(error);
    }

    public onCompleted(): void {
        this._onCompleted();
    }
}