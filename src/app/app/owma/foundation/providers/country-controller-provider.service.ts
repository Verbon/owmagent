import { Injectable } from '@angular/core';
import { Country } from '../../domain-model/country';
import { CountryControllerFactory } from '../factories/country-controller-factory.service';
import { ICountryController } from '../interfaces/i-country-controller';
import { CachingEntityControllerProvider } from './caching-entity-controller-provider';


@Injectable()
export class CountryControllerProvider extends CachingEntityControllerProvider<Country, ICountryController> {
    public constructor(countryControllerFactory: CountryControllerFactory) {
        super(countryControllerFactory);
    }
}