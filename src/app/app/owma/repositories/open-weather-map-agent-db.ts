import Dexie from 'dexie';
import { Entity } from '../../common/domain-model/entity';
import { IDexieDb } from '../../common/repositories/dexie/i-dexie-db';
import { City } from '../domain-model/city';
import { Country } from '../domain-model/country';
import { WeatherInfo } from '../domain-model/weather-info';


export class OpenWeatherMapAgentDb extends Dexie implements IDexieDb {
    private static readonly databaseName: string = "OpenWeatherMapAgentDatabase";

    private readonly _openWeatherMapAgentTables: Map<string, object>;


    public cities: Dexie.Table<City, number>;

    public countries: Dexie.Table<Country, number>;

    public weatherHistory: Dexie.Table<WeatherInfo, number>;


    public constructor() {
        super(OpenWeatherMapAgentDb.databaseName);

        this._openWeatherMapAgentTables = new Map<string, object>([
            [City.name, () => this.cities],
            [Country.name, () => this.countries],
            [WeatherInfo.name, () => this.weatherHistory]
        ]);
    }


    public getTable<T extends Entity>(type: new () => T): Dexie.Table<T, number> {
        let tableProvider = <() => Dexie.Table<T, number>>this._openWeatherMapAgentTables.get(type.name);
        let table = tableProvider();

        return table;
    }
}