import { app, BrowserWindow, ipcMain } from 'electron';


class App {
    private static readonly splashScreenWindowUrl: string = `file://${__dirname}/splash-screen/splash-screen.html`;
    private static readonly mainWindowUrl: string = `file://${__dirname}/app/index.html`;
    private static readonly applicationInitializedEventName: string = 'application-initialized';

    private static _splashScreen: Electron.BrowserWindow | null;
    private static _mainWindow: Electron.BrowserWindow | null;


    public static run(): void {
        app.on('ready', App.onReady);
        app.on('window-all-closed', App.onWindowAllClosed);
    }


    private static onReady(): void {
        App.loadSplashScreen();
        App.subscribeToAppInitializedEvent();
        App.loadMainWindow();
    }

    private static onWindowAllClosed(): void {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    }

    private static loadSplashScreen(): void {
        App._splashScreen = App.createBrowserWindow(600, 400, false, true);
        App._splashScreen.on('closed', () => App._splashScreen = null);
        App._splashScreen.on('ready-to-show', () => (<Electron.BrowserWindow>App._splashScreen).show());

        App._splashScreen.loadURL(App.splashScreenWindowUrl);
    }

    private static loadMainWindow(): void {
        App._mainWindow = App.createBrowserWindow(1024, 768, false);
        App._mainWindow.on('closed', () => App._mainWindow = null);

        App._mainWindow.loadURL(App.mainWindowUrl);
    }

    private static createBrowserWindow(width: number, height: number, shouldShow: boolean = true, frameless: boolean = false): Electron.BrowserWindow {
        return new BrowserWindow({
            width: width,
            height: height,
            show: shouldShow,
            frame: !frameless
        });
    }

    private static subscribeToAppInitializedEvent(): void {
        ipcMain.on(App.applicationInitializedEventName, App.onAppInitialized);
    }

    private static onAppInitialized(): void {
        (<Electron.BrowserWindow>App._splashScreen).close();
        (<Electron.BrowserWindow>App._mainWindow).show();
    }
}


App.run();