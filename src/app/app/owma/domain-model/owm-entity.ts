import { Entity } from '../../common/domain-model/entity';


export abstract class OwmEntity extends Entity {
    public owmId: number;
}