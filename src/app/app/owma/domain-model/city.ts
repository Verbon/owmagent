import { OwmEntity } from './owm-entity';


export class City extends OwmEntity {
    public name: string;

    public countryId: number;

    public longitude: number;

    public latitude: number;
}