import { Inject, Injectable } from '@angular/core';
import { IObservable } from '../../../common/observable/i-observable';
import { City } from '../../domain-model/city';
import { Country } from '../../domain-model/country';
import { CountriesObservableInjectionToken } from '../../owma.core.module.configuration';
import { OpenWeatherMapAgentUnitOfWork } from '../../repositories/open-weather-map-agent-unit-of-work';
import { EntitiesChangedEventArgs } from '../entity-controller-service/entities-changed-event-args';
import { EntityControllerServiceBase } from '../entity-controller-service/entity-controller-service-base';
import { ICityController } from '../interfaces/i-city-controller';
import { ICountryController } from '../interfaces/i-country-controller';
import { IInternalCountryService } from '../interfaces/i-internal-country-service';
import { CityControllerProvider } from '../providers/city-controller-provider.service';
import { CountryControllerProvider } from '../providers/country-controller-provider.service';


@Injectable()
export class CountryService extends EntityControllerServiceBase<Country, ICountryController> implements IInternalCountryService {
    private readonly _owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork;
    private readonly _cityControllerProvider: CityControllerProvider;
    private readonly _countryControllerProvider: CountryControllerProvider;


    public constructor(
        owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork,
        cityControllerProvider: CityControllerProvider,
        countryControllerProvider: CountryControllerProvider,
        @Inject(CountriesObservableInjectionToken)
        observable: IObservable<EntitiesChangedEventArgs<Country>>) {
        super(countryControllerProvider, observable, Country);

        this._owmaUnitOfWork = owmaUnitOfWork;
        this._cityControllerProvider = cityControllerProvider;
        this._countryControllerProvider = countryControllerProvider;
    }


    public async getCountriesAsync(): Promise<ICountryController[]> {
        let countryRepository = this._owmaUnitOfWork.getRepository(Country);
        let dbCountries = await countryRepository.getAllAsync();
        let countriesControllers = await Promise.all(dbCountries.map(c => this._countryControllerProvider.getControllerForAsync(c)));

        return countriesControllers;
    }

    public async getCitiesAsync(country: Country): Promise<ICityController[]> {
        let cityRepository = this._owmaUnitOfWork.getRepository(City);
        let dbCities = await cityRepository.getWhereAsync(c => c.countryId === country.id);
        let citiesControllers = await Promise.all(dbCities.map(c => this._cityControllerProvider.getControllerForAsync(c)));

        return citiesControllers;
    }
}