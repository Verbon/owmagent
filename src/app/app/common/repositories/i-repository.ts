import { Entity } from '../domain-model/entity';


export interface IRepository<T extends Entity> {
    getAllAsync(): Promise<T[]>;

    getWhereAsync(predicate: (entity: T) => boolean): Promise<T[]>;

    singleAsync(predicate?: (entity: T) => boolean): Promise<T>;

    singleOrDefaultAsync(predicate?: (entity: T) => boolean): Promise<T | null>;

    addAsync(entity: T): Promise<void>;

    addAllAsync(entities: T[]): Promise<void>;

    updateAsync(entity: T): Promise<void>;

    updateAllAsync(entities: T[]): Promise<void>;

    deleteAsync(entity: T): Promise<void>;

    deleteAsync(entities: T[]): Promise<void>;

    deleteAllAsync(): Promise<void>;
}