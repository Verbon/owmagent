import { NgModule } from '@angular/core';
import { ApplicationConfigurationService } from './configuration/application-configuration.service';
import { JsonSerializer } from './serialization/json/json-serializer.service';
import { FileManager } from './system-utilities/file-manager.service';


@NgModule({
    providers: [
        JsonSerializer,
        ApplicationConfigurationService,
        FileManager
    ]
})
export class CoreModule {

}