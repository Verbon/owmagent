import { Entity } from '../../../common/domain-model/entity';
import { IEntityController } from './i-entity-controller';


export interface IEntityControllerProvider<TEntity extends Entity, TEntityController extends IEntityController<TEntity>> {
    getControllerForAsync(entity: TEntity): Promise<TEntityController>;

    getEntityOf(entityController: TEntityController): TEntity;
}