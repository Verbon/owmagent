import { City } from '../../domain-model/city';
import { ICountryController } from './i-country-controller';
import { IEntityController } from './i-entity-controller';
import { IWeatherInfoController } from './i-weather-info-controller';


export interface ICityController extends IEntityController<City> {
    readonly name: string;

    readonly latitude: number;

    readonly longitude: number;

    readonly country: ICountryController;


    getWeatherHistoryAsync(): Promise<IWeatherInfoController[]>;
}