export interface ServerCity {
    readonly id: number;

    readonly latitude: number;

    readonly longitude: number;

    readonly name: string;
}