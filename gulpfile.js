var gulp = require('gulp');
var ts = require('gulp-typescript');
var run = require('gulp-run');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var runSequence = require('run-sequence');

generateTasksFor('dev');
generateTasksFor('prod');

function generateTasksFor(configuration) {
    gulp.task(`rebuild-${configuration}`, function () {
        runSequence(`clean-${configuration}`, `build-${configuration}`);
    });

    gulp.task(`clean-${configuration}`, function () {
        return gulp.src(`./bin/${configuration}`, { read: false })
            .pipe(clean());
    });

    gulp.task(`build-${configuration}`,
        [`build-angular-${configuration}`, `build-splash-screen-${configuration}`, `embed-resources-${configuration}`, `config-${configuration}`],
        function () {
            var tsProject = ts.createProject('./config/tsconfig.json');

            return gulp.src('./src/*.ts')
                .pipe(tsProject()).js
                .pipe(gulp.dest(`./bin/${configuration}`));
        });

    gulp.task(`build-angular-${configuration}`, function () {
        return run(`webpack --config config/angular2/webpack.${configuration}.js`).exec();
    });

    gulp.task(`build-splash-screen-${configuration}`,
        [`compile-splash-screen-code-behind-${configuration}`, `compile-splash-screen-styles-${configuration}`],
        function () {
            return gulp.src('./src/splash-screen/*.html')
                .pipe(gulp.dest(`./bin/${configuration}/splash-screen`));
        });

    gulp.task(`embed-resources-${configuration}`, function() {
        return gulp.src('./src/assets/**')
            .pipe(gulp.dest(`./bin/${configuration}/assets`));
    });

    gulp.task(`config-${configuration}`, function () {
        return gulp.src(`./src/app.${configuration}.config.json`)
            .pipe(rename('app.config.json'))
            .pipe(gulp.dest(`./bin/${configuration}`));
    });

    gulp.task(`compile-splash-screen-code-behind-${configuration}`, function () {
        var tsProject = ts.createProject('./config/tsconfig.json');

        return gulp.src('./src/splash-screen/*.ts')
            .pipe(tsProject()).js
            .pipe(gulp.dest(`./bin/${configuration}/splash-screen`));
    });

    gulp.task(`compile-splash-screen-styles-${configuration}`, function () {
        return gulp.src('./src/splash-screen/*.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest(`./bin/${configuration}/splash-screen`));
    });
}