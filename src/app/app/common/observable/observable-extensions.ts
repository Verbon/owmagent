import { ParameterizedAction } from '../action';
import { IDisposable } from '../disposing/i-disposable';
import { AnonymousObserver } from './anonymous-observer';
import { IObservable } from './i-observable';


export class ObservableExtensions {
    private constructor() {

    }


    public static subscribe<T>(observable: IObservable<T>, subscriber: ParameterizedAction<T>, parameter?: object): IDisposable {
        return observable.subscribe(new AnonymousObserver<T>(subscriber), parameter);
    }
}