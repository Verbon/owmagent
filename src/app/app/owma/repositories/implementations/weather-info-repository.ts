import * as _ from 'lodash';
import { Repository } from '../../../common/repositories/dexie/repository';
import { City } from '../../domain-model/city';
import { WeatherInfo } from '../../domain-model/weather-info';
import { IWeatherInfoRepository } from '../interfaces/i-weather-info-repository';


export class WeatherInfoRepository extends Repository<WeatherInfo> implements IWeatherInfoRepository {
    public async getCityLatestWeatherInfoAsync(city: City): Promise<WeatherInfo | null> {
        let cityWeatherHistory = await this.table
            .where('cityId')
            .equals(city.id)
            .reverse()
            .sortBy('dateTime');

        return _.head(cityWeatherHistory) || null;
    }
}