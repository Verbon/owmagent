import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { IDeactivationAware } from './i-deactivation-aware';


@Injectable()
export class CanDeactivateGuard implements CanDeactivate<IDeactivationAware> {
    public canDeactivate(component: IDeactivationAware): boolean | Observable<boolean> | Promise<boolean> {
        return component.canDeactivate();
    }
}