import { OperationResult } from '../../../common/operation-result';
import { ICityController } from './i-city-controller';
import { IWeatherInfoController } from './i-weather-info-controller';


export interface IWeatherService {
    getCityLatestWeatherAsync(cityName: string): Promise<OperationResult<IWeatherInfoController>>;

    getCityLatestWeatherAsync(cityController: ICityController): Promise<OperationResult<IWeatherInfoController>>;
}