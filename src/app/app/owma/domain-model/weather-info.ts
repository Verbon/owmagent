import { Entity } from '../../common/domain-model/entity';


/**
 * temperature - Kelvin,
 * pressure - hPa,
 * humidity - %,
 * wind speed - m/s,
 * clouds - %,
 * dateTime - unix timestamp,
 * sunrise - unix timestamp,
 * sunset - unix timestamp
 */
export class WeatherInfo extends Entity {
    public cityId: number;

    public temperature: number;

    public pressure: number;

    public humidity: number;

    public windSpeed: number;

    public clouds: number;

    public dateTime: number;

    public sunrise: number;

    public sunset: number;
}