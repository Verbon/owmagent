import { Inject, Injectable } from '@angular/core';
import { IObservable } from '../../../common/observable/i-observable';
import { City } from '../../domain-model/city';
import { Country } from '../../domain-model/country';
import { WeatherInfo } from '../../domain-model/weather-info';
import { CitiesObservableInjectionToken } from '../../owma.core.module.configuration';
import { OpenWeatherMapAgentUnitOfWork } from '../../repositories/open-weather-map-agent-unit-of-work';
import { EntitiesChangedEventArgs } from '../entity-controller-service/entities-changed-event-args';
import { EntityControllerServiceBase } from '../entity-controller-service/entity-controller-service-base';
import { ICityController } from '../interfaces/i-city-controller';
import { ICountryController } from '../interfaces/i-country-controller';
import { IInternalCityService } from '../interfaces/i-internal-city-service';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';
import { CityControllerProvider } from '../providers/city-controller-provider.service';
import { CountryControllerProvider } from '../providers/country-controller-provider.service';
import { WeatherInfoControllerProvider } from '../providers/weather-info-controller-provider.service';


@Injectable()
export class CityService extends EntityControllerServiceBase<City, ICityController> implements IInternalCityService {
    private readonly _owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork;
    private readonly _cityControllerProvider: CityControllerProvider;
    private readonly _countryControllerProvider: CountryControllerProvider;
    private readonly _weatherInfoControllerProvider: WeatherInfoControllerProvider;


    public constructor(
        owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork,
        cityControllerProvider: CityControllerProvider,
        countryControllerProvider: CountryControllerProvider,
        weatherInfoControllerProvider: WeatherInfoControllerProvider,
        @Inject(CitiesObservableInjectionToken)
        observable: IObservable<EntitiesChangedEventArgs<City>>) {
        super(cityControllerProvider, observable, City);

        this._owmaUnitOfWork = owmaUnitOfWork;
        this._cityControllerProvider = cityControllerProvider;
        this._countryControllerProvider = countryControllerProvider;
        this._weatherInfoControllerProvider = weatherInfoControllerProvider;
    }


    public async getCitiesAsync(): Promise<ICityController[]> {
        let cityRepository = this._owmaUnitOfWork.getRepository(City);
        let dbCities = await cityRepository.getAllAsync();
        let citiesControllers = await Promise.all(dbCities.map(c => this._cityControllerProvider.getControllerForAsync(c)));

        return citiesControllers;
    }

    public async getCountryAsync(city: City): Promise<ICountryController> {
        let countryRepository = this._owmaUnitOfWork.getRepository(Country);
        let dbCountry = await countryRepository.singleAsync(c => c.id === city.countryId);
        let countryController = await this._countryControllerProvider.getControllerForAsync(dbCountry);

        return countryController;
    }

    public async getWeatherHistoryAsync(city: City): Promise<IWeatherInfoController[]> {
        let weatherInfoRepository = this._owmaUnitOfWork.getRepository(WeatherInfo);
        let dbWeatherInfos = await weatherInfoRepository.getWhereAsync(wi => wi.cityId === city.id);
        let weatherInfosControllers = await Promise.all(dbWeatherInfos.map(wi => this._weatherInfoControllerProvider.getControllerForAsync(wi)));

        return weatherInfosControllers;
    }
}