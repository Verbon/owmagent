import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ipcRenderer } from 'electron';
import { AppModule } from './app/app.module';

const applicationInitializedEventName: string = 'application-initialized';


if(process.env.ENV === 'production') {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule).then(() => ipcRenderer.send(applicationInitializedEventName));