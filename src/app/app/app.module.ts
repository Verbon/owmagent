import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import * as path from 'path';
import { AppRoutingModule } from './app.routing';
import { ApplicationConfigurationService } from './core/configuration/application-configuration.service';
import { OwmaModule } from './owma/owma.module';
import { SharedModule } from './shared/shared.module';
import { ShellComponent } from './shell.component';


@NgModule({
    imports: [
        SharedModule,
        BrowserModule,
        OwmaModule,
        AppRoutingModule
    ],
    providers: [
        { provide: APP_INITIALIZER, useFactory: (appModule: AppModule) => () => appModule.initializeApp(), deps: [AppModule], multi: true }
    ],
    declarations: [
        ShellComponent
    ],
    bootstrap: [
        ShellComponent
    ]
})
export class AppModule {
    private static readonly appConfigAbsolutePath: string = path.join(__dirname, '../app.config.json');

    private readonly _applicationConfigurationService: ApplicationConfigurationService;


    public constructor(applicationConfigurationService: ApplicationConfigurationService) {
        this._applicationConfigurationService = applicationConfigurationService;
    }


    public initializeApp(): void {
        this._applicationConfigurationService.initialize(AppModule.appConfigAbsolutePath);
    }
}