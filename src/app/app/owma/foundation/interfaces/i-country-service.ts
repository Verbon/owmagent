import { Country } from '../../domain-model/country';
import { ICountryController } from './i-country-controller';
import { IEntityControllerService } from './i-entity-controller-service';


export interface ICountryService extends IEntityControllerService<Country, ICountryController> {
    getCountriesAsync(): Promise<ICountryController[]>;
}