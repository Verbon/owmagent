import { Func } from '../func';


export class Lazy<T> {
    private readonly _valueFactory: Func<T>;

    private _value: T;


    public get value(): T {
        return this._value ? this._value : (this._value = this._valueFactory());
    }


    public constructor(valueFactory: Func<T>) {
        this._valueFactory = valueFactory;
    }
}