import { Dexie } from 'dexie';
import { Entity } from '../../domain-model/entity';
import { DatabaseInitializer } from '../database-initializer';
import { IRepository } from '../i-repository';
import { IDexieDb } from './i-dexie-db';


export class Repository<T extends Entity> implements IRepository<T> {
    private readonly _entityType: new () => T;
    private readonly _db: IDexieDb;


    protected get table(): Dexie.Table<T, number> { return this._db.getTable(this._entityType); }


    public constructor(type: new () => T, db: IDexieDb) {
        this._entityType = type;
        this._db = db;
    }


    public async getAllAsync(): Promise<T[]> {
        await this.prepareAsync();

        return await this.table.toArray();
    }

    public async getWhereAsync(predicate: (entity: T) => boolean): Promise<T[]> {
        await this.prepareAsync();

        return await this.table.filter(predicate).toArray();
    }

    public async singleAsync(predicate?: (entity: T) => boolean): Promise<T> {
        await this.prepareAsync();

        let entity = await this.singleOrDefaultAsync(predicate);
        if (entity === null) {
            throw new Error("No matching elements.");
        }

        return entity;
    }

    public async singleOrDefaultAsync(predicate?: (entity: T) => boolean): Promise<T | null> {
        await this.prepareAsync();

        let entities = predicate
            ? await this.getWhereAsync(predicate)
            : await this.getAllAsync();

        if (entities.length > 1) {
            throw new Error("Found multiple matching elements.");
        }
        else if (entities.length < 1) {
            return null;
        }
        else {
            return entities[0];
        }
    }

    public async addAsync(entity: T): Promise<void> {
        await this.prepareAsync();

        await this.table.add(entity);
    }

    public async addAllAsync(entities: T[]): Promise<void> {
        await this.prepareAsync();

        await this.table.bulkAdd(entities);
    }

    public async updateAsync(entity: T): Promise<void> {
        await this.prepareAsync();

        await this.table.put(entity);
    }

    public async updateAllAsync(entities: T[]): Promise<void> {
        await this.prepareAsync();

        await this.table.bulkPut(entities);
    }

    public async deleteAsync(entity: T): Promise<void>;
    public async deleteAsync(entities: T[]): Promise<void>;
    public async deleteAsync(entityOrEntities: any): Promise<void> {
        await this.prepareAsync();

        if (Array.isArray(entityOrEntities)) {
            let entities = <T[]>entityOrEntities;

            let entitiesIdsToDelete = entities.map(e => e.id);
            await this.table.bulkDelete(entitiesIdsToDelete);
        }
        else {
            let entity = <T>entityOrEntities;

            await this.table.delete(entity.id);
        }
    }

    public async deleteAllAsync(): Promise<void> {
        await this.prepareAsync();

        await this.table.clear();
    }


    private async prepareAsync(): Promise<void> {
        await DatabaseInitializer.initializeAsync();
    }
}