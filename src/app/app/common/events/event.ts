import * as _ from 'lodash';
import { EventArgs } from './event-args';
import { EventHandler } from './event-handler';


export interface IEvent<T extends EventArgs> {
    subscribe(subscriber: object, eventHandler: EventHandler<T>): void;

    unsubscribe(subscriber: object, eventHandler: EventHandler<T>): void;
}

export class Event<T extends EventArgs> implements IEvent<T> {
    private readonly _eventSubscriptions: EventSubscription<T>[];


    public constructor() {
        this._eventSubscriptions = [];
    }


    public subscribe(subscriber: object, eventHandler: EventHandler<T>): void {
        let eventSubscription: EventSubscription<T> = { subscriber: subscriber, eventHandler: eventHandler };

        this._eventSubscriptions.push(eventSubscription);
    }

    public unsubscribe(subscriber: object, eventHandler: EventHandler<T>): void {
        _.remove(this._eventSubscriptions, es => es.subscriber === subscriber && es.eventHandler === eventHandler);
    }

    public raise(sender: object, e: T): void {
        this._eventSubscriptions.slice(0).forEach(es => es.eventHandler.call(es.subscriber, sender, e));
    }
}


interface EventSubscription<T> {
    subscriber: object;

    eventHandler: EventHandler<T>;
}