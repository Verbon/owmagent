import * as moment from 'moment';
import { WeatherInfo } from '../../domain-model/weather-info';
import { ICityController } from './i-city-controller';
import { IEntityController } from './i-entity-controller';


export interface IWeatherInfoController extends IEntityController<WeatherInfo> {
    readonly city: ICityController;

    readonly temperature: number;

    readonly pressure: number;

    readonly humidityPercentage: number;

    readonly windSpeed: number;

    readonly cloudsPercentage: number;

    readonly dateTime: moment.Moment;

    readonly sunrise: moment.Moment;

    readonly sunset: moment.Moment;
}