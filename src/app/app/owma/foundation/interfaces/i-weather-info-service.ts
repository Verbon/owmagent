import { WeatherInfo } from '../../domain-model/weather-info';
import { IEntityControllerService } from './i-entity-controller-service';
import { IWeatherInfoController } from './i-weather-info-controller';


export interface IWeatherInfoService extends IEntityControllerService<WeatherInfo, IWeatherInfoController> {

}