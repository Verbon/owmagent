import { IDisposable } from '../disposing/i-disposable';
import { IObserver } from './i-observer';


export interface IObservable<T> {
    subscribe(observer: IObserver<T>, parameter?: object): IDisposable;
}