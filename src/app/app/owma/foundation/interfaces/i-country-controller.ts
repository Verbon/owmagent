import { Country } from '../../domain-model/country';
import { ICityController } from './i-city-controller';
import { IEntityController } from './i-entity-controller';


export interface ICountryController extends IEntityController<Country> {
    readonly code: string;


    getCitiesAsync(): Promise<ICityController[]>;
}