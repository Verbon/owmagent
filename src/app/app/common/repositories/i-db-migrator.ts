export interface IDbMigrator {
    migrateAsync(): Promise<void>;
}