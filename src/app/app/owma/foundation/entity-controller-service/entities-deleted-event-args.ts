import { EventArgs } from '../../../common/events/event-args';


export class EntitiesDeletedEventArgs<TEntityController> extends EventArgs {
    public constructor(
        public readonly entityControllers: TEntityController[]) {
        super();
    }
}