import { EventArgs } from '../../../common/events/event-args';


export class EntitiesAddedEventArgs<TEntityController> extends EventArgs {
    public constructor(
        public readonly entityControllers: TEntityController[]) {
        super();
    }
}