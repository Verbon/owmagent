import { Injectable } from '@angular/core';
import * as http from 'http';
import { ResponseStatusCodes } from '../../common/networking/http/response-status-codes';
import { OperationResult } from '../../common/operation-result';
import { JsonSerializer } from '../../core/serialization/json/json-serializer.service';
import { OwmaFoundationConfigManager } from '../foundation/configuration/owma-foundation-config-manager.service';
import { GetCityLatestWeatherResponseDataContract } from './data-contracts/get-city-latest-weather-response-data-contract';
import { GetCityLatestWeatherServerResponse } from './server-responses/get-city-latest-weather-server-response';


@Injectable()
export class OwmaWebService {
    private static readonly contentTypeHeaderName: string = 'content-type';
    private static readonly jsonContentTypeRegEx: RegExp = /^application\/json/;
    private static readonly weatherUrl: string = 'weather';
    private static readonly queryParametersKeys = { cityName: 'q', cityId: 'id', appId: 'APPID' };


    private readonly _owmaFoundationConfigManager: OwmaFoundationConfigManager;
    private readonly _jsonSerializer: JsonSerializer;


    public constructor(owmaFoundationConfigManager: OwmaFoundationConfigManager, jsonSerializer: JsonSerializer) {
        this._owmaFoundationConfigManager = owmaFoundationConfigManager;
        this._jsonSerializer = jsonSerializer;
    }


    public async getCityLatestWeatherAsync(cityName: string): Promise<OperationResult<GetCityLatestWeatherServerResponse>>;
    public async getCityLatestWeatherAsync(cityId: number): Promise<OperationResult<GetCityLatestWeatherServerResponse>>;
    public async getCityLatestWeatherAsync(nameOrId: any): Promise<OperationResult<GetCityLatestWeatherServerResponse>> {
        let queryParams: [string, string][];
        if (typeof nameOrId === 'string') {
            let cityName = <string>nameOrId;
            queryParams = [[OwmaWebService.queryParametersKeys.cityName, cityName]];
        } else {
            let cityId = <number>nameOrId;
            queryParams = [[OwmaWebService.queryParametersKeys.cityId, cityId.toString()]];
        }
        let getCityCurrentWeatherUrl = this.buildUrl(OwmaWebService.weatherUrl, queryParams);

        return await this.getAsync<GetCityLatestWeatherServerResponse, GetCityLatestWeatherResponseDataContract>(getCityCurrentWeatherUrl, GetCityLatestWeatherServerResponse);
    }


    private getAsync<TServerResponse, TResponseDataContract>(
        requestUrl: string,
        serverResponse: new (responseDataContract: TResponseDataContract) => TServerResponse): Promise<OperationResult<TServerResponse>> {
        return new Promise<OperationResult<TServerResponse>>(resolve => {
            http.get(requestUrl, incomingMessage => {
                const statusCode = incomingMessage.statusCode;
                const contentType = <string>incomingMessage.headers[OwmaWebService.contentTypeHeaderName];

                let error;
                if (statusCode !== ResponseStatusCodes.ok) {
                    error = new Error(`Request failed. Status code: ${statusCode}.`);
                } else if (!OwmaWebService.jsonContentTypeRegEx.test(contentType)) {
                    error = new Error(`Invalid Content-Type. Expected application/json, but received ${contentType}`);
                }
                if (error) {
                    incomingMessage.resume(); // consume response data to free up memory
                    resolve(OperationResult.createUnsuccessful<TServerResponse>());

                    return;
                }

                incomingMessage.setEncoding('utf8');
                let rawData = '';
                incomingMessage.on('data', (chunk) => { rawData += chunk; });
                incomingMessage.on('end', () => {
                    try {
                        let responseDataContract = this._jsonSerializer.deserialize<TResponseDataContract>(rawData);

                        resolve(OperationResult.createSuccessful<TServerResponse>(new serverResponse(responseDataContract)));
                    } catch (e) {
                        console.log(e);
                        resolve(OperationResult.createUnsuccessful<TServerResponse>());
                    }
                });
            }).on('error', e => {
                console.log(e);
                resolve(OperationResult.createUnsuccessful<TServerResponse>());
            });
        });
    }

    private buildUrl(url: string, queryParams?: [string, string][]): string {
        let resultUrl = new URL(url, this._owmaFoundationConfigManager.openWeatherMapUrl);
        if (queryParams) {
            for (let queryParam of queryParams) {
                let [name, value] = queryParam;
                resultUrl.searchParams.append(name, value);
            }
        }
        resultUrl.searchParams.append(OwmaWebService.queryParametersKeys.appId, this._owmaFoundationConfigManager.openWeatherMapApiKey);

        return resultUrl.href;
    }
}