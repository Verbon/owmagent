import { Entity } from '../../../common/domain-model/entity';
import { IEntityController } from './i-entity-controller';


export interface IEntityAwareEntityController<TEntity extends Entity> extends IEntityController<TEntity> {
    readonly innerEntity: TEntity;
}