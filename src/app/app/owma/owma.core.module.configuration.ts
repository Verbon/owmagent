import { InjectionToken } from '@angular/core';
import { Lazy } from '../common/lazy/lazy';
import { IObservable } from '../common/observable/i-observable';
import { City } from './domain-model/city';
import { Country } from './domain-model/country';
import { WeatherInfo } from './domain-model/weather-info';
import { EntitiesChangedEventArgs } from './foundation/entity-controller-service/entities-changed-event-args';
import { ICityService } from './foundation/interfaces/i-city-service';
import { ICountryService } from './foundation/interfaces/i-country-service';
import { IInternalCityService } from './foundation/interfaces/i-internal-city-service';
import { IInternalCountryService } from './foundation/interfaces/i-internal-country-service';
import { IInternalWeatherInfoService } from './foundation/interfaces/i-internal-weather-info-service';
import { IWeatherInfoService } from './foundation/interfaces/i-weather-info-service';
import { IWeatherService } from './foundation/interfaces/i-weather-service';


export const WeatherServiceInjectionToken = new InjectionToken<IWeatherService>('WeatherService');

export const CityServiceInjectionToken = new InjectionToken<ICityService>('CityService');
export const InternalCityServiceInjectionToken = new InjectionToken<IInternalCityService>('InternalCityService');
export const LazyInternalCityServiceInjectionToken = new InjectionToken<Lazy<IInternalCityService>>('LazyInternalCityService');

export const CountryServiceInjectionToken = new InjectionToken<ICountryService>('CountryService');
export const InternalCountryServiceInjectionToken = new InjectionToken<IInternalCountryService>('InternalCountryService');
export const LazyInternalCountryServiceInjectionToken = new InjectionToken<Lazy<IInternalCountryService>>('LazyInternalCountryService');

export const WeatherInfoServiceInjectionToken = new InjectionToken<IWeatherInfoService>('WeatherInfoService');
export const InternalWeatherInfoServiceInjectionToken = new InjectionToken<IInternalWeatherInfoService>('InternalWeatherInfoService');
export const LazyInternalWeatherInfoServiceInjectionToken = new InjectionToken<Lazy<IInternalWeatherInfoService>>('LazyInternalWeatherInfoService');

export const CitiesObservableInjectionToken = new InjectionToken<IObservable<EntitiesChangedEventArgs<City>>>('CitiesObservable');
export const CountriesObservableInjectionToken = new InjectionToken<IObservable<EntitiesChangedEventArgs<Country>>>('CountriesObservable');
export const WeatherInfosObservableInjectionToken = new InjectionToken<IObservable<EntitiesChangedEventArgs<WeatherInfo>>>('WeatherInfosObservable');
