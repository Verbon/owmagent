import { Inject, Injectable } from '@angular/core';
import { Lazy } from '../../../common/lazy/lazy';
import { City } from '../../domain-model/city';
import { LazyInternalCityServiceInjectionToken } from '../../owma.core.module.configuration';
import { CityController } from '../controllers/city-controller';
import { ICityController } from '../interfaces/i-city-controller';
import { IEntityControllerFactory } from '../interfaces/i-entity-controller-factory';
import { IInternalCityService } from '../interfaces/i-internal-city-service';


@Injectable()
export class CityControllerFactory implements IEntityControllerFactory<City, ICityController> {
    private readonly _lazyInternalCityService: Lazy<IInternalCityService>;


    private get internalCityService(): IInternalCityService {
        return this._lazyInternalCityService.value;
    }


    public constructor(
        @Inject(LazyInternalCityServiceInjectionToken)
        lazyInternalCityService: Lazy<IInternalCityService>) {
        this._lazyInternalCityService = lazyInternalCityService;
    }


    public async createFromAsync(city: City): Promise<ICityController> {
        let cityController = new CityController(this.internalCityService);
        await cityController.initializeAsync(city);

        return cityController;
    }
}