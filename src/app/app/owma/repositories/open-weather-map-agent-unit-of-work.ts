import { Injectable } from '@angular/core';
import { Entity } from '../../common/domain-model/entity';
import { IDexieDb } from '../../common/repositories/dexie/i-dexie-db';
import { Repository } from '../../common/repositories/dexie/repository';
import { UnitOfWork } from '../../common/repositories/dexie/unit-of-work';
import { IDbMigration } from '../../common/repositories/i-db-migration';
import { IRepository } from '../../common/repositories/i-repository';
import { WeatherInfo } from '../domain-model/weather-info';
import { WeatherInfoRepository } from './implementations/weather-info-repository';
import { IWeatherInfoRepository } from './interfaces/i-weather-info-repository';
import { InitialCreate } from './migrations/initial-create_201708031625';
import { OpenWeatherMapAgentDb } from './open-weather-map-agent-db';


@Injectable()
export class OpenWeatherMapAgentUnitOfWork extends UnitOfWork {
    private readonly _repositoryTypes: Map<string, object>;


    public get weatherInfoRepository(): IWeatherInfoRepository { return <IWeatherInfoRepository>this.getRepository(WeatherInfo); }


    public constructor() {
        let db = new OpenWeatherMapAgentDb();
        let migrations = OpenWeatherMapAgentUnitOfWork.createMigrations(db);

        super(db, migrations);

        this._repositoryTypes = OpenWeatherMapAgentUnitOfWork.createRepositoriesTypesMapping();
    }


    protected createRepository<T extends Entity>(type: new () => T): IRepository<T> {
        if (this._repositoryTypes.has(type.name)) {
            let repositoryType = <new (type: new () => T, db: IDexieDb) => Repository<T>>this._repositoryTypes.get(type.name);

            return new repositoryType(type, this.db);
        }

        return super.createRepository(type);
    }


    private static createRepositoriesTypesMapping(): Map<string, object> {
        return new Map<string, object>(
            [[WeatherInfo.name, WeatherInfoRepository]]
        );
    }

    private static createMigrations(db: OpenWeatherMapAgentDb): IDbMigration[] {
        return [
            new InitialCreate(db)
        ];
    }
}