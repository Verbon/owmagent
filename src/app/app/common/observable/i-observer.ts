export interface IObserver<T> {
    onNext(value: T): void;

    onError(error: Error): void;

    onCompleted(): void;
}