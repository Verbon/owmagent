import { City } from '../../domain-model/city';
import { ICityController } from './i-city-controller';
import { IEntityControllerService } from './i-entity-controller-service';


export interface ICityService extends IEntityControllerService<City, ICityController> {
    getCitiesAsync(): Promise<ICityController[]>;
}