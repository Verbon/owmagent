export interface Action {
    (): void;
}

export interface ParameterizedAction<T> {
    (arg: T): void;
}