import { Country } from '../../domain-model/country';
import { ICityController } from './i-city-controller';
import { ICountryService } from './i-country-service';


export interface IInternalCountryService extends ICountryService {
    getCitiesAsync(country: Country): Promise<ICityController[]>;
}