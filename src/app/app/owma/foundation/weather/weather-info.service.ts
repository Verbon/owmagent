import { Inject, Injectable } from '@angular/core';
import { IObservable } from '../../../common/observable/i-observable';
import { City } from '../../domain-model/city';
import { WeatherInfo } from '../../domain-model/weather-info';
import { WeatherInfosObservableInjectionToken } from '../../owma.core.module.configuration';
import { OpenWeatherMapAgentUnitOfWork } from '../../repositories/open-weather-map-agent-unit-of-work';
import { EntitiesChangedEventArgs } from '../entity-controller-service/entities-changed-event-args';
import { EntityControllerServiceBase } from '../entity-controller-service/entity-controller-service-base';
import { ICityController } from '../interfaces/i-city-controller';
import { IInternalWeatherInfoService } from '../interfaces/i-internal-weather-info-service';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';
import { CityControllerProvider } from '../providers/city-controller-provider.service';
import { WeatherInfoControllerProvider } from '../providers/weather-info-controller-provider.service';


@Injectable()
export class WeatherInfoService extends EntityControllerServiceBase<WeatherInfo, IWeatherInfoController> implements IInternalWeatherInfoService {
    private readonly _owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork;
    private readonly _cityControllerProvider: CityControllerProvider;


    public constructor(
        owmaUnitOfWork: OpenWeatherMapAgentUnitOfWork,
        cityControllerProvider: CityControllerProvider,
        weatherInfoControllerProvider: WeatherInfoControllerProvider,
        @Inject(WeatherInfosObservableInjectionToken)
        observable: IObservable<EntitiesChangedEventArgs<WeatherInfo>>) {
        super(weatherInfoControllerProvider, observable, WeatherInfo);
        
        this._owmaUnitOfWork = owmaUnitOfWork;
        this._cityControllerProvider = cityControllerProvider;
    }


    public async getCityAsync(weatherInfo: WeatherInfo): Promise<ICityController> {
        let cityRepository = this._owmaUnitOfWork.getRepository(City);
        let dbCity = await cityRepository.singleAsync(c => c.id === weatherInfo.cityId);
        let cityController = await this._cityControllerProvider.getControllerForAsync(dbCity);

        return cityController;
    }
}