import { InjectionToken, Injector, NgModule } from '@angular/core';
import { Lazy } from '../common/lazy/lazy';
import { CoreModule } from '../core/core.module';
import { CityService } from './foundation/cities/city.service';
import { CountryService } from './foundation/countries/country.service';
import { CityControllerFactory } from './foundation/factories/city-controller-factory.service';
import { CountryControllerFactory } from './foundation/factories/country-controller-factory.service';
import { WeatherInfoControllerFactory } from './foundation/factories/weather-info-controller-factory.service';
import { CityControllerProvider } from './foundation/providers/city-controller-provider.service';
import { CountryControllerProvider } from './foundation/providers/country-controller-provider.service';
import { WeatherInfoControllerProvider } from './foundation/providers/weather-info-controller-provider.service';
import { WeatherInfoService } from './foundation/weather/weather-info.service';
import { WeatherService } from './foundation/weather/weather.service';
import {
    CitiesObservableInjectionToken,
    CityServiceInjectionToken,
    CountriesObservableInjectionToken,
    CountryServiceInjectionToken,
    InternalCityServiceInjectionToken,
    InternalCountryServiceInjectionToken,
    InternalWeatherInfoServiceInjectionToken,
    LazyInternalCityServiceInjectionToken,
    LazyInternalCountryServiceInjectionToken,
    LazyInternalWeatherInfoServiceInjectionToken,
    WeatherInfoServiceInjectionToken,
    WeatherInfosObservableInjectionToken
    } from './owma.core.module.configuration';
import { WeatherServiceInjectionToken } from './owma.core.module.configuration';
import { OpenWeatherMapAgentUnitOfWork } from './repositories/open-weather-map-agent-unit-of-work';
import { OwmaWebService } from './service-clients/owma-web.service';
import { OwmaFoundationConfigManager } from './foundation/configuration/owma-foundation-config-manager.service';


@NgModule({
    imports: [
        CoreModule
    ],
    providers: [
        OpenWeatherMapAgentUnitOfWork,

        OwmaFoundationConfigManager,

        OwmaWebService,
        WeatherService,
        { provide: WeatherServiceInjectionToken, useExisting: WeatherService },
        { provide: CitiesObservableInjectionToken, useExisting: WeatherService },
        { provide: CountriesObservableInjectionToken, useExisting: WeatherService },
        { provide: WeatherInfosObservableInjectionToken, useExisting: WeatherService },

        CityService,
        { provide: CityServiceInjectionToken, useExisting: CityService },
        { provide: InternalCityServiceInjectionToken, useExisting: CityService },
        {
            provide: LazyInternalCityServiceInjectionToken,
            useFactory: (injector: Injector) => createLazy(injector, InternalCityServiceInjectionToken),
            deps: [Injector]
        },
        CityControllerFactory,
        CityControllerProvider,

        CountryService,
        { provide: CountryServiceInjectionToken, useExisting: CountryService },
        { provide: InternalCountryServiceInjectionToken, useExisting: CountryService },
        {
            provide: LazyInternalCountryServiceInjectionToken,
            useFactory: (injector: Injector) => createLazy(injector, InternalCountryServiceInjectionToken),
            deps: [Injector]
        },
        CountryControllerFactory,
        CountryControllerProvider,

        WeatherInfoService,
        { provide: WeatherInfoServiceInjectionToken, useExisting: WeatherInfoService },
        { provide: InternalWeatherInfoServiceInjectionToken, useExisting: WeatherInfoService },
        {
            provide: LazyInternalWeatherInfoServiceInjectionToken,
            useFactory: (injector: Injector) => createLazy(injector, InternalWeatherInfoServiceInjectionToken),
            deps: [Injector]
        },
        WeatherInfoControllerFactory,
        WeatherInfoControllerProvider
    ]
})
export class OwmaCoreModule {

}


function createLazy<T>(injector: Injector, injectionToken: InjectionToken<T>): Lazy<T> {
    return new Lazy<T>(() => injector.get(injectionToken));
}