import { GetCityLatestWeatherResponseDataContract } from '../data-contracts/get-city-latest-weather-response-data-contract';
import { ServerCity } from './server-city';
import { ServerCountry } from './server-country';


export class GetCityLatestWeatherServerResponse {
    public readonly country: ServerCountry;

    public readonly city: ServerCity;

    public readonly cloudsPercentage: number;

    public readonly calculationUnixTimeUTC: number;

    public readonly humidityPercentage: number;

    public readonly pressure: number;

    public readonly temperature: number;

    public readonly windSpeed: number;

    public readonly sunriseUnixTimeUTC: number;

    public readonly sunsetUnixTimeUTC: number;


    public constructor(dataContract: GetCityLatestWeatherResponseDataContract) {
        this.country = { code: dataContract.sys.country };
        this.city = {
            id: dataContract.id,
            latitude: dataContract.coord.lat,
            longitude: dataContract.coord.lon,
            name: dataContract.name
        };
        this.cloudsPercentage = dataContract.clouds.all;
        this.calculationUnixTimeUTC = dataContract.dt;
        this.humidityPercentage = dataContract.main.humidity;
        this.pressure = dataContract.main.pressure;
        this.temperature = dataContract.main.temp;
        this.windSpeed = dataContract.wind.speed;
        this.sunriseUnixTimeUTC = dataContract.sys.sunrise;
        this.sunsetUnixTimeUTC = dataContract.sys.sunset;
    }
}