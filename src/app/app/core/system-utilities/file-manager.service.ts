import { Injectable } from '@angular/core';
import * as fs from 'fs';


@Injectable()
export class FileManager {
    public readFileAsStringAsync(filePath: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            fs.readFile(filePath, (error, data) => {
                if (error) {
                    reject(error);

                    return;
                }

                let fileContentAsString = data.toString();
                resolve(fileContentAsString);
            });
        });
    }
}