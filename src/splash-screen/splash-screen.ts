const { app } = require('electron').remote;


document.addEventListener('DOMContentLoaded', function() {
    (<HTMLElement>document.getElementById('app-version')).textContent = app.getVersion();

    let now = new Date();
    (<HTMLElement>document.getElementById('current-year')).textContent = now.getUTCFullYear().toString();
});