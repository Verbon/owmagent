import { Entity } from '../../../common/domain-model/entity';
import { Event, IEvent } from '../../../common/events/event';
import { IObservable } from '../../../common/observable/i-observable';
import { ObservableExtensions } from '../../../common/observable/observable-extensions';
import { IEntityController } from '../interfaces/i-entity-controller';
import { IEntityControllerProvider } from '../interfaces/i-entity-controller-provider';
import { IEntityControllerService } from '../interfaces/i-entity-controller-service';
import { EntitiesAddedEventArgs } from './entities-added-event-args';
import { EntitiesChangedEventArgs } from './entities-changed-event-args';
import { EntitiesDeletedEventArgs } from './entities-deleted-event-args';
import { EntitiesUpdatedEventArgs } from './entities-updated-event-args';
import { EntityChangeAction } from './entity-change-action';


export abstract class EntityControllerServiceBase<TEntity extends Entity, TEntityController extends
    IEntityController<TEntity>> implements IEntityControllerService<TEntity, TEntityController> {
    private readonly _entityControllerProvider: IEntityControllerProvider<TEntity, TEntityController>;

    private readonly _entitiesAddedEvent = new Event<EntitiesAddedEventArgs<TEntityController>>();
    private readonly _entitiesUpdatedEvent = new Event<EntitiesUpdatedEventArgs<TEntityController>>();
    private readonly _entitiesDeletedEvent = new Event<EntitiesDeletedEventArgs<TEntityController>>();


    public get entitiesAdded(): IEvent<EntitiesAddedEventArgs<TEntityController>> { return this._entitiesAddedEvent; }

    public get entitiesUpdated(): IEvent<EntitiesUpdatedEventArgs<TEntityController>> { return this._entitiesUpdatedEvent; }

    public get entitiesDeleted(): IEvent<EntitiesDeletedEventArgs<TEntityController>> { return this._entitiesDeletedEvent; }


    protected constructor(
        entityControllerProvider: IEntityControllerProvider<TEntity, TEntityController>,
        observable: IObservable<EntitiesChangedEventArgs<TEntity>>,
        entityType: new () => TEntity) {
        this._entityControllerProvider = entityControllerProvider;

        ObservableExtensions.subscribe(observable, e => this.handleEntitiesChanged(e), entityType);
    }


    private handleEntitiesChanged(e: EntitiesChangedEventArgs<TEntity>): void {
        this.onEntitiesChangedAsync(e.entities, e.entityChangeAction);
    }

    private async onEntitiesChangedAsync(entities: TEntity[], entityChangeAction: EntityChangeAction): Promise<void> {
        let entityControllers = await Promise.all(entities.map(e => this._entityControllerProvider.getControllerForAsync(e)));
        switch (entityChangeAction) {
            case EntityChangeAction.Add:
                this.onEntitiesAdded(entityControllers);
                break;
            case EntityChangeAction.Update:
                this.onEntitiesUpdated(entityControllers);
                break;
            case EntityChangeAction.Delete:
                this.onEntitiesDeleted(entityControllers);
                break;
            default:
                throw new Error(`Argument out of range ${entityChangeAction}.`);
        }
    }

    private onEntitiesAdded(entityControllers: TEntityController[]): void {
        this._entitiesAddedEvent.raise(this, new EntitiesAddedEventArgs(entityControllers));
    }

    private onEntitiesUpdated(entityControllers: TEntityController[]): void {
        this._entitiesUpdatedEvent.raise(this, new EntitiesUpdatedEventArgs(entityControllers));
    }

    private onEntitiesDeleted(entityControllers: TEntityController[]): void {
        this._entitiesDeletedEvent.raise(this, new EntitiesDeletedEventArgs(entityControllers));
    }
}