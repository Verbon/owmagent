export enum EntityChangeAction {
    Add,
    Update,
    Delete
}