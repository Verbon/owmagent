import { City } from '../../domain-model/city';
import { ICityService } from './i-city-service';
import { ICountryController } from './i-country-controller';
import { IWeatherInfoController } from './i-weather-info-controller';


export interface IInternalCityService extends ICityService {
    getCountryAsync(city: City): Promise<ICountryController>;

    getWeatherHistoryAsync(city: City): Promise<IWeatherInfoController[]>;
}