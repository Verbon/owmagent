import { Inject, Injectable } from '@angular/core';
import { Lazy } from '../../../common/lazy/lazy';
import { WeatherInfo } from '../../domain-model/weather-info';
import { LazyInternalWeatherInfoServiceInjectionToken } from '../../owma.core.module.configuration';
import { WeatherInfoController } from '../controllers/weather-info-controller';
import { IEntityControllerFactory } from '../interfaces/i-entity-controller-factory';
import { IInternalWeatherInfoService } from '../interfaces/i-internal-weather-info-service';
import { IWeatherInfoController } from '../interfaces/i-weather-info-controller';


@Injectable()
export class WeatherInfoControllerFactory implements IEntityControllerFactory<WeatherInfo, IWeatherInfoController> {
    private readonly _lazyInternalWeatherInfoService: Lazy<IInternalWeatherInfoService>;


    private get internalWeatherInfoService(): IInternalWeatherInfoService {
        return this._lazyInternalWeatherInfoService.value;
    }


    public constructor(
        @Inject(LazyInternalWeatherInfoServiceInjectionToken)
        lazyInternalWeatherInfoService: Lazy<IInternalWeatherInfoService>) {
        this._lazyInternalWeatherInfoService = lazyInternalWeatherInfoService;
    }


    public async createFromAsync(weatherInfo: WeatherInfo): Promise<IWeatherInfoController> {
        let weatherInfoController = new WeatherInfoController(this.internalWeatherInfoService);
        await weatherInfoController.initializeAsync(weatherInfo);

        return weatherInfoController;
    }
}